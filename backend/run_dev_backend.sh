#!/usr/bin/env bash

cd /work/backend
pip install -e .
./manage.py migrate --no-input
./manage.py runserver '0.0.0.0:8000'
