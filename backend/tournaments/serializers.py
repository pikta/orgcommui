import secrets

from rest_framework import serializers

from .models import Tournament
from .models import InstitutionRegistration
from .models import Team
from .models import Participant


class TournamentSerializer(serializers.ModelSerializer):
    accessibility_schema = serializers.SlugRelatedField(
        read_only=True, slug_field="schema"
    )

    class Meta:
        model = Tournament
        fields = [
            "name",
            "slug",
            "allow_changes",
            "allow_ia",
            "allow_independent_teams",
            "allow_independent_participants",
            "accessibility_schema",
        ]


class ParticipantInitialSerializer(serializers.ModelSerializer):
    slug = serializers.HiddenField(default=secrets.token_urlsafe)

    class Meta:
        model = Participant
        fields = ["email", "slug"]


class IndependentTeamSerializer(serializers.ModelSerializer):
    slug = serializers.HiddenField(default=secrets.token_urlsafe)
    participants = ParticipantInitialSerializer(many=True)

    class Meta:
        model = Team
        fields = ["name", "slug", "participants"]


class TeamSerializer(serializers.ModelSerializer):
    participants = ParticipantInitialSerializer(many=True)

    class Meta:
        model = Team
        fields = ["name", "participants"]


class InstitutionRegistrationSerializer(serializers.ModelSerializer):
    slug = serializers.HiddenField(default=secrets.token_urlsafe)
    teams = TeamSerializer(many=True)

    class Meta:
        model = InstitutionRegistration
        fields = ["name", "email", "slug", "teams", "comments"]


class ParticipantFullSerializer(serializers.ModelSerializer):
    class Meta:
        model = Participant
        fields = [
            "refer_name",
            "refer_pronouns",
            "legal_name",
            "legal_pronouns",
            "to_parent_name",
            "to_parent_pronouns",
            "date_of_birth",
            "emergency_contacts",
            "accessibility_data",
            "is_judge",
        ]
