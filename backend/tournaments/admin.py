from django.contrib import admin

from .models import AccessibilitySchema
from .models import Tournament
from .models import InstitutionRegistration
from .models import Team
from .models import Participant


admin.site.register(AccessibilitySchema)
admin.site.register(Tournament)
admin.site.register(InstitutionRegistration)
admin.site.register(Team)
admin.site.register(Participant)
