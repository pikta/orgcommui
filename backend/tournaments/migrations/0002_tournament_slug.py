# Generated by Django 4.2.2 on 2023-06-26 06:55

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("tournaments", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="tournament",
            name="slug",
            field=models.CharField(default="aaa", max_length=255),
            preserve_default=False,
        ),
    ]
