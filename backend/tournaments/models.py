from django.db import models


class AccessibilitySchema(models.Model):
    name = models.CharField(max_length=255)
    schema = models.JSONField()


class Tournament(models.Model):
    name = models.CharField(max_length=255)
    slug = models.CharField(max_length=255)
    allow_display = models.BooleanField(default=False)
    allow_changes = models.BooleanField(default=False)
    allow_ia = models.BooleanField(default=True)
    allow_independent_teams = models.BooleanField(default=False)
    allow_independent_participants = models.BooleanField(default=False)
    accessibility_schema = models.ForeignKey(
        AccessibilitySchema, null=True, on_delete=models.SET_NULL
    )


class InstitutionRegistration(models.Model):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    status = models.CharField(max_length=255)
    slug = models.CharField(max_length=255)
    comments = models.TextField()


class Team(models.Model):
    name = models.CharField(max_length=255)
    institution = models.ForeignKey(
        InstitutionRegistration, null=True, on_delete=models.CASCADE
    )


class Participant(models.Model):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    institution = models.ForeignKey(
        InstitutionRegistration, null=True, on_delete=models.CASCADE
    )
    team = models.ForeignKey(Team, null=True, on_delete=models.CASCADE)
    email = models.EmailField()
    slug = models.CharField(max_length=255)
    refer_name = models.CharField(max_length=255, null=True)
    refer_pronouns = models.CharField(max_length=255, null=True)
    legal_name = models.CharField(max_length=255, null=True)
    legal_pronouns = models.CharField(max_length=255, null=True)
    to_parent_name = models.CharField(max_length=255, null=True)
    to_parent_pronouns = models.CharField(max_length=255, null=True)
    date_of_birth = models.DateField(null=True)
    emergency_contacts = models.TextField(null=True)
    accessibility_data = models.JSONField(null=True)
    is_judge = models.BooleanField(default=False)
