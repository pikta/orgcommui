from django.urls import path

from .views import TournamentList
from .views import TournamentRetrieve
from .views import InstitutionRegister
from .views import InstitutionUpdate
from .views import TeamRegister
from .views import TeamUpdate
from .views import ParticipantRegister
from .views import ParticipantUpdate

urlpatterns = [
    path("", TournamentList.as_view()),
    path("<slug:slug>", TournamentRetrieve.as_view()),
    path("<slug:tourney_slug>/new_institution", InstitutionRegister.as_view()),
    path("<slug:tourney_slug>/new_team", TeamRegister.as_view()),
    path("<slug:tourney_slug>/new_participant", ParticipantRegister.as_view()),
    path("institution/<slug:slug>", InstitutionUpdate.as_view()),
    path("team/<slug:slug>", TeamUpdate.as_view()),
    path("participant/<slug:slug>", ParticipantUpdate.as_view()),
]
