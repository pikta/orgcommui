from django.shortcuts import get_object_or_404
from rest_framework import generics

from .models import Tournament
from .serializers import TournamentSerializer
from .serializers import InstitutionRegistrationSerializer
from .serializers import IndependentTeamSerializer
from .serializers import TeamSerializer
from .serializers import ParticipantFullSerializer


class TournamentList(generics.ListAPIView):
    queryset = Tournament.objects.filter(allow_display=True)
    serializer_class = TournamentSerializer


class TournamentRetrieve(generics.RetrieveAPIView):
    queryset = Tournament.objects.filter(allow_display=True)
    serializer_class = TournamentSerializer
    lookup_field = "slug"


class InstitutionRegister(generics.CreateAPIView):
    serializer_class = InstitutionRegistrationSerializer

    def perform_create(self, serializer):
        tournament = get_object_or_404(Tournament, slug=self.kwargs["tourney_slug"])
        serializer.save(tournament=tournament)


class InstitutionUpdate(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = InstitutionRegistrationSerializer
    lookup_field = "slug"

    def perform_create(self, serializer):
        tournament = get_object_or_404(Tournament, slug=self.kwargs["tourney_slug"])
        serializer.save(tournament=tournament)


class TeamRegister(generics.CreateAPIView):
    serializer_class = IndependentTeamSerializer


class TeamUpdate(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TeamSerializer
    lookup_field = "slug"


class ParticipantRegister(generics.CreateAPIView):
    serializer_class = ParticipantFullSerializer

    def perform_create(self, serializer):
        tournament = get_object_or_404(Tournament, slug=self.kwargs["tourney_slug"])
        serializer.save(tournament=tournament)


class ParticipantUpdate(generics.RetrieveUpdateAPIView):
    serializer_class = ParticipantFullSerializer
    lookup_field = "slug"
