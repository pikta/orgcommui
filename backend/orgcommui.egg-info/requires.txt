Django==4.2.2
djangorestframework==3.14.0
psycopg2==2.9.6
requests==2.31.0

[devel]
black
pyyaml
uritemplate

[prod]
gunicorn
