// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: [
    '@/custom.scss',
  ],
  devtools: { enabled: true },
  modules: [
    '@nuxtjs/tailwindcss',
  ],
  nitro: {
    devProxy: {
      '/api': 'http://localhost:8000/',
    },
  },
  ssr: false,
})
